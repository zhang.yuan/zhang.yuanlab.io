---
layout: page
title: AirExp
description: AirExp is a prettier interface to explore the data from the OurAirports database. 
redirect: https://airexp.voyez.eu/
img: /assets/img/airexp.jpg
importance: 1
category: fun
---