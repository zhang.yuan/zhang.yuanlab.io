---
layout: page
title: jupyter docker
description: A docker image with pre-installed jupyter libraries and extensions.
redirect: https://github.com/nyradr/jupyter-docker
img:
importance: 3
category: fun
---