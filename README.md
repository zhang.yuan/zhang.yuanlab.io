# voyez.eu

This site is currently deployed at [https://voyez.eu](https://voyez.eu) and is based on a customised [al-folio](https://github.com/alshedivat/al-folio) template.

On top of the al-folio features, this site features a CV generation system and a [polyglot](https://polyglot.untra.io/) integration (for french and english).

## How to edit

All the content is either defined in the `_config.yml` or in the `_data/:lang/*.yml` files. Edit the content of thoses files to change the content of the website.

## How to use

See [jekyll](https://jekyllrb.com/) for the jekyll setup.

- Build the dependencies
```
bundler install
```

- Launch the dev server
```
bundle exec jekyll serve
```

- Deploy the production version:
```
bundle exec jekyll build
```
(for [ovh](https://www.ovh.com/fr/hebergement-web/) individual hosting)
The previous command will create a `_site` directory containing all the website files. All you need to do is to remove all the files in the host `www` directory and ftp copy the content of this directory into the `www` directory of the host.